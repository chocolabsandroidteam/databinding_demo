package testing.usagi.databinding;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;

import testing.usagi.databinding.databinding.ActivityMainBinding;
import testing.usagi.databinding.databinding.AdapterRevealBinding;
import testing.usagi.databinding.databinding.CharacterBinding;
import testing.usagi.databinding.model.BasicModel;
import testing.usagi.databinding.model.BasicObservableModel;
import testing.usagi.databinding.model.SecBasicModel;
import testing.usagi.databinding.model.SecBasicObservableModel;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mainBinding.setBasicModel(getBasicModel());
        mainBinding.setSecBasicModel(getSecBasicModel());
        mainBinding.btn1.setOnClickListener(view ->{
            AdapterRevealBinding adapterRevealBinding = DataBindingUtil.setContentView(this, R.layout.adapter_reveal);
            adapterRevealBinding.container.setBinding(adapterRevealBinding);
        });

        // ================================================
        CharacterBinding characterBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.character, null, false);
        ((LinearLayout)mainBinding.getRoot()).addView(characterBinding.getRoot());

        BasicObservableModel basicObservableModel = getBasicObservableModel();
        characterBinding.setBaseObservableModel(basicObservableModel);
        characterBinding.setSecBaseObservableModel(getSecBasicObservableModel());

        characterBinding.changeBtn1.setOnClickListener(view -> basicObservableModel.setAge("30"));
        characterBinding.changeBtn2.setOnClickListener(view -> characterBinding.getSecBaseObservableModel().age.set(
            characterBinding.getSecBaseObservableModel().age.get() + 20));
    }

    private BasicModel getBasicModel(){
        BasicModel model = new BasicModel();
        model.setTitle("TTTTTTTTTTTTT");
        model.setStartTime("SSSSSSSSSSSSS");
        model.setDuration("DDDDDDDDDDDD");

        return model;
    }

    private SecBasicModel getSecBasicModel(){
        SecBasicModel model = new SecBasicModel();
        model.setUserName("Yeeeeeeeeee");
        model.setId(Integer.toString(9527));
        return model;
    }

    private BasicObservableModel getBasicObservableModel(){
        BasicObservableModel model = new BasicObservableModel();
        model.setCharacter("Hero");
        model.setAge("20");
        model.setAttackRange("100");
        model.setSex("male");

        return model;
    }

    private SecBasicObservableModel getSecBasicObservableModel(){
        SecBasicObservableModel model = new SecBasicObservableModel();
        model.character.set("Akuma");
        model.age.set(200);
        model.sex.set("Unknown");
        model.attackRange.set(1000);

        return model;
    }
}
