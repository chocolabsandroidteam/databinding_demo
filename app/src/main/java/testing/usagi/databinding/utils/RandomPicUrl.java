package testing.usagi.databinding.utils;

import java.util.Random;

/**
 * Created by Usagi on 16/5/16.
 */
public class RandomPicUrl {
    private static final Random random = new Random();
    private static final String[] urls = {
        "http://orig05.deviantart.net/fd07/f/2012/243/0/7/neo_steam___art_3_by_kyoung_seok-d5d1yis.jpg",
        "http://www.blogcdn.com/massively.joystiq.com/media/2009/05/neosteamloadingscreenimageat580.jpg",
        "http://pre07.deviantart.net/9d4a/th/pre/f/2013/290/d/f/neo_steam__background__by_cursedblade1337-d6qufyc.png",
        "http://i.kinja-img.com/gawker-media/image/upload/s--tMukmaOO--/18j4xkwgqhodkjpg.jpg",
        "https://images4.alphacoders.com/377/37706.jpg",
        "http://pre15.deviantart.net/ed81/th/pre/i/2012/241/2/a/neo_steam___art_1_by_kyoung_seok-d5cuago.jpg",
        "https://s-media-cache-ak0.pinimg.com/736x/a7/d9/9a/a7d99a6b01eee391958cf3c515e61e4e.jpg",
        "https://s-media-cache-ak0.pinimg.com/736x/15/1c/07/151c0709a1e49f21ce0852209b9045e6.jpg",
        "https://s-media-cache-ak0.pinimg.com/736x/c8/e5/0a/c8e50a3df2d9fec9f212005038a6a00e.jpg"
    };

    static public String randomUrl(){
        return urls[random.nextInt(urls.length)];
    }
}
