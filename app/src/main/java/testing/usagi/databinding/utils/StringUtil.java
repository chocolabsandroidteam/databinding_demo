package testing.usagi.databinding.utils;

/**
 * Created by Usagi on 16/5/11.
 */
public class StringUtil {

    static public String toUpperCase(String str){
        return str == null ? "" : str.toUpperCase();
    }

    static public String toLowerCase(String str){
        return str == null ? "" :  str.toLowerCase();
    }
}
