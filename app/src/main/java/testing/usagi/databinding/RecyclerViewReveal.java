package testing.usagi.databinding;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import testing.usagi.databinding.databinding.AdapterRevealBinding;
import testing.usagi.databinding.model.ItemInfoModel;

/**
 * Created by Usagi on 16/5/13.
 */
public class RecyclerViewReveal extends RelativeLayout{
    private AdapterRevealBinding adapterRevealBinding;
    public RecyclerViewReveal(Context context) {
        super(context, null);
    }

    public RecyclerViewReveal(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RecyclerViewReveal(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
    }

    public void setBinding(AdapterRevealBinding binding){
        adapterRevealBinding = binding;
        RecyclerView recyclerView = adapterRevealBinding.recyclerView;
        RecycleViewAdapter adapter = new RecycleViewAdapter(getContext());
        adapter.setItemInfoList(getItemInfoList());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
    }

    private List<ItemInfoModel> getItemInfoList(){
        List<ItemInfoModel> itemInfoList = new ArrayList<ItemInfoModel>();
        itemInfoList.add(new ItemInfoModel("Zero", "4"));
        itemInfoList.add(new ItemInfoModel("RockMen", "5"));
        itemInfoList.add(new ItemInfoModel("Lily", "3"));

        return itemInfoList;
    }
}
