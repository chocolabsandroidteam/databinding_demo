package testing.usagi.databinding;

import android.util.Log;

/**
 * Created by Usagi on 16/5/16.
 */
public class MyLog {
    final static boolean isOpen = true;
    public static void v(String tag, String msg){
        if(isOpen) {
            Log.v(tag, msg);
        }
    }
}
