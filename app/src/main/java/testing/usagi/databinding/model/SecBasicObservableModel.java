package testing.usagi.databinding.model;

import android.databinding.ObservableField;
import android.databinding.ObservableInt;

/**
 * Created by Usagi on 16/5/11.
 */
public class SecBasicObservableModel {
    public final ObservableField<String> character = new ObservableField<>();
    public final ObservableField<String> sex = new ObservableField<>();
    public final ObservableInt age = new ObservableInt();
    public final ObservableInt attackRange = new ObservableInt();
}
