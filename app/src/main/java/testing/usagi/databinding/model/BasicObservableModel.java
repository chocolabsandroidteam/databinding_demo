package testing.usagi.databinding.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import testing.usagi.databinding.BR;

/**
 * Created by Usagi on 16/5/11.
 */
public class BasicObservableModel extends BaseObservable {
    private String character;
    private String sex;
    private String age;
    private String attackRange;

    @Bindable
    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
        notifyPropertyChanged(BR.character);
    }

    @Bindable
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
        notifyPropertyChanged(BR.sex);
    }

    @Bindable
    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
        notifyPropertyChanged(BR.age);
    }

    @Bindable
    public String getAttackRange() {
        return attackRange;
    }

    public void setAttackRange(String attackRange) {
        this.attackRange = attackRange;
        notifyPropertyChanged(BR.attackRange);
    }
}
