package testing.usagi.databinding.model;

/**
 * Created by Usagi on 16/5/11.
 */
public class SecBasicModel {
    private String userName;
    private String id;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
