package testing.usagi.databinding.model;

/**
 * Created by Usagi on 16/5/11.
 */
public class BasicModel {
    private String title;
    private String startTime;
    private String duration;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
