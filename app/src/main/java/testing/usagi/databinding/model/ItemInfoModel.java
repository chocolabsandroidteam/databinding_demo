package testing.usagi.databinding.model;

/**
 * Created by Usagi on 16/5/16.
 */
public class ItemInfoModel {
    private String itemName;
    private  String itemRank;

    public ItemInfoModel(String itemName, String itemRank) {
        this.itemName = itemName;
        this.itemRank = itemRank;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemRank() {
        return itemRank;
    }

    public void setItemRank(String itemRank) {
        this.itemRank = itemRank;
    }
}
