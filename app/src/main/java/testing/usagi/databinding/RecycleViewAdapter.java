package testing.usagi.databinding;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import testing.usagi.databinding.databinding.ListItemBinding;
import testing.usagi.databinding.model.ItemInfoModel;
import testing.usagi.databinding.utils.RandomPicUrl;

/**
 * Created by Usagi on 16/5/16.
 */
public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.ViewHolder> {
    private List<ItemInfoModel> itemInfoList;
    public View.OnClickListener onTest = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            MyLog.v(ViewHolder.class.getSimpleName(), "CLICKED!!!");
        }
    };

    public RecycleViewAdapter(Context context) {
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, null));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(itemInfoList != null) {
            holder.bindModel(itemInfoList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return itemInfoList == null ? 0 : itemInfoList.size();
    }

    public void setItemInfoList(List<ItemInfoModel> itemInfoList){
        this.itemInfoList = itemInfoList;
    }

    @BindingAdapter({"bind:imgUrl", "bind:error"})
    public static void loadImg(ImageView imgView, String imgUrl, Drawable err){
        Glide.with(imgView.getContext()).load(imgUrl).error(err).into(imgView);
    }

    // --------------------- ViewHolder ---------------------
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ListItemBinding listItemBinding;

        public View.OnClickListener onChangeImg = imgView->{
            listItemBinding.setImgUrl(RandomPicUrl.randomUrl());
        };

        public ViewHolder(View itemView) {
            super(itemView);
            listItemBinding = ListItemBinding.bind(itemView);
        }

        public void bindModel(ItemInfoModel itemInfoModel) {
            listItemBinding.setItemInfoModel(itemInfoModel);
            listItemBinding.setImgUrl(RandomPicUrl.randomUrl());
            listItemBinding.setViewholder(this);
        }

        public ListItemBinding getBinding() {
            return listItemBinding;
        }
    }
}
